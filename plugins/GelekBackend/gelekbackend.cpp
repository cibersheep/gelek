#include "gelekbackend.h"
#include <QDebug>
#include <QDir>
#include <QObject>
#include <QProcess>
//Jujuyeh
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QString>

#include <iostream>
#include <cstring>

#include <QStandardPaths>

GelekBackend::GelekBackend() {
}

const QString savedGamesPath = (QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
const QString gamesPath = (QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/Games/");
const QString gamesFilesPath = (QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/GameFiles/");

//TODO: Define constant to be used in qml
/*
const int cacheLocation = 1;
const int appConfigLocation = 2;
const int appLocalDataLocation = 3;
*/

void GelekBackend::systemInfo() {
    qDebug() << "CacheLocation:        " << QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    qDebug() << "AppConfigLocation:    " << QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "ConfigLocation:       " << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    qDebug() << "AppConfigLocation:    " << QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "TempLocation:         " << QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    qDebug() << "AppLocalDataLocation: " << QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
}

bool GelekBackend::launch(const QString &game) {
    m_process = new QProcess(this);
    m_process->setReadChannel(QProcess::StandardOutput);

    //Connect the QProcess signals to the code (precessOutput emits a signal to qml)
    connect (m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(processOutput()));

    QStringList args;
    args << game;

    m_process->start("terps/lev9glk", args);
    qDebug() << "Load lev9glk and game: " << game;

    if (!m_process->waitForStarted()) {
            qDebug() << "Levevl9 could not start. Failed to load " << args;
            return false;
    }
    else {
        qDebug() << "Level9 started";
        return true;
    }
}

void GelekBackend::stopTerp() {
    if (m_process->QProcess::Running) {
        qDebug() << "Level9 was running. Terminating...";
        m_process->terminate();
        qDebug() << "Is finished?: " << m_process->waitForFinished();

        //We don't need the pointer anymore
        delete m_process;
        qDebug() << "And m_process deleted";
    }
    else qDebug() << "Level9 was not running";
}
void GelekBackend::processOutput() {
    //If you need to do json manipulation, this is a good place to do it
    emit stdoutLevel();
}

void GelekBackend::sendCommand(const QString &cmd) {
    if (m_process->state() != QProcess::Running)
        return;
    //qDebug() << "[toGlk] " << cmd;

    m_process->write(cmd.toLocal8Bit() + '\n');
}

QString GelekBackend::readSOL() {
    QByteArray bytes = m_process ->readAllStandardOutput();
    QString output = QString::fromLocal8Bit(bytes);
    
    //DEBUG qDebug() << "------ stdout output\n" << output;
    //DEBUG qDebug() << "\n------------------";

    return output;
}

QString GelekBackend::storeGameFile(QString gameURL, QString gameName) {
    QDir storeDir;

    //Should be /home/phablet/.cache/gelek.cibersheep/Games/
    storeDir = gamesPath;

    QFile copyGame;
    QDir storeGameDir;
    storeGameDir.mkpath(storeDir.absolutePath());
    //storeGameDir.setCurrent(storeDir);
    copyGame.copy(gameURL, storeDir.filePath(gameName));
    qDebug() << "gameName cpp: " << storeDir.filePath(gameName);
    
    //Return path of the stored game
    return storeDir.filePath(gameName);
}

void GelekBackend::createSavedGamesDir() {
    QDir savedGamesDir;

    //Should be /home/phablet/.cache/gelek.cibersheep/SavedGames/
    savedGamesDir = savedGamesPath;
    savedGamesDir.mkpath(savedGamesDir.absolutePath());
}

void GelekBackend::deleteSavedGames(QString savedGame) {
    QFile removeSavedGame;

    qDebug() << "Deleting: " << savedGame;
    if (!removeSavedGame.remove(savedGame)) {
        qDebug() << "Error deleting file";
    }
}

QStringList GelekBackend::listFiles() {
	/*
	// Use, in qml, launch.listFiles() or FolderListModel
    QString gamesDir;
    gamesDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/HubIncoming/";

    QStringList filters;
    filters << "*.dat" << "*.l9" << "*.sna" << "*.DAT" << "*.L9" << "*.SNA" << "*.st" << "*.ST";

    QStringList gamesFiles;

    //QDirIterator it(dir, QStringList() << "*.dat" << "*.l9" << "*.sna", QDir::NoFilter, QDirIterator::Subdirectories);
    QDirIterator it(gamesDir, filters, QDir::NoFilter, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        gamesFiles.append(it.next());
    }

    //qDebug() << gamesFiles;
    return gamesFiles;
    */
}

using namespace std;

void GelekBackend::addToList(const QString savefile, const QString game, const QString dateTime) {
    QDir appDir;
    appDir.cd(savedGamesPath);

    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {return;}
    QTextStream toList(&saveList);
    toList << '"' << savefile.toUtf8().data() << "\": { game:\"" << game.toUtf8().data()
           << "\", time:\"" << dateTime.toUtf8().data() << '"' << " }" << endl;
    saveList.close();
}

QString GelekBackend::whichGame(const QString savefile) {
    QDir appDir;
    appDir.cd(savedGamesPath);
    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::ReadOnly | QIODevice::Text)) {return "";}
    QTextStream fromList(&saveList);
    QString searchString(savefile);
    QString line;
    QString game;
    line = fromList.readLine();
    //qDebug() << "line: -----------" + line;
    //qDebug() << "--------- line: " << line << " isNull? " << line.isNull();
    while (!line.isNull()) {
        //qDebug() << "--------- while";
        //qDebug() << "--------- line contains: " << line.contains(searchString, Qt::CaseSensitive);
        //qDebug() << "---------  searchString: " << searchString;
        //line = fromList.readLine();
        if (line.contains(searchString, Qt::CaseSensitive)) {
            line.remove(0,savefile.size()+12);
            QString gameEnd;
            gameEnd = '"';
            QStringRef y = line.midRef(0, line.indexOf(gameEnd));
            game.append(y);
            saveList.close();
            return game;
        }
        line = fromList.readLine();
    }
    saveList.close();
    game.append("Unkown Game");
    return game;
}

void GelekBackend::whatTime(const QString savefile, QString dateTime) {
    QDir appDir;
    appDir.cd(savedGamesPath);
    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::ReadOnly | QIODevice::Text)) {return;}
    QTextStream fromList(&saveList);
    QString searchString(savefile);
    QString line;
    while (!line.isNull()) {
        line = fromList.readLine();
        if (line.contains(searchString, Qt::CaseSensitive)) {
            QString timeSearch = "time:";
            line.remove(0,line.indexOf(timeSearch) + 6);
            line.chop(1);
            dateTime.append(line);
            saveList.close();
            return;
        }
    }
    saveList.close();
}

void GelekBackend::deleteSave(const QString savefile) {

    QDir appDir;
    appDir.cd(savedGamesPath);

    QString renameFrom;
    QString renameTo;

    renameFrom.append(savedGamesPath);
    renameFrom.append("gelekSaveList");

    renameTo.append(savedGamesPath);
    renameTo.append("gelekSaveListOLD");
    qDebug() << renameFrom << "  " << renameTo;

    rename(renameFrom.toUtf8(), renameTo.toUtf8());

    QFile saveListOLD(appDir.filePath("gelekSaveListOLD"));
    if (!saveListOLD.open(QIODevice::ReadOnly | QIODevice::Text)) {return;}
    QTextStream fromList(&saveListOLD);

    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {qDebug() << "Couldn't open gelekSavedList"; return;}
    QTextStream toList(&saveList);

    QString line;
    //QString saveListContent;
    line = fromList.readLine();
    qDebug() << "Is Line Null? " << line.isNull();
    while (!line.isNull()) {
        qDebug() << "Line: " << line;
        if (!(line.contains(savefile, Qt::CaseSensitive))) {
            toList << line << endl;
        }
        line = fromList.readLine();
    }
    saveList.close();
    saveListOLD.close();
}

QStringList GelekBackend::exportSave() {
    QStringList exportedFile;

    QDir appDir;
    appDir.cd(savedGamesPath);

    QString savedGamesList;

    savedGamesList.append(savedGamesPath);
    savedGamesList.append("gelekSaveList");

    QFile saveList(appDir.filePath("gelekSaveList"));

    if (!saveList.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Couldn't open gelekSavedList";
        return {};
    }

    QTextStream readList(&saveList);

    QString line;

    line = readList.readLine();
    qDebug() << "Is Line Null? " << line.isNull();

    while (!line.isNull()) {
        qDebug() << "Line: " << line;

        exportedFile << line;
        line = readList.readLine();
    }

    saveList.close();

    return exportedFile;
}

QString GelekBackend::standardPathTo(int path) {
    switch(path)
    {
        case 1 :  return QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
                  break;
        case 2 :  return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
                  break;
        case 3 :  return QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
                  break;

    }
}

void GelekBackend::firtRunFolderSetUp() {
    //Create a game folder
    QDir initFolders;

    //TODO: This returns boolean if folders created successfully. Use that
    initFolders.mkpath(savedGamesPath);
    initFolders.mkpath(gamesPath);
    initFolders.mkpath(gamesFilesPath);
}
