#!/bin/bash

#Build libraries
echo "Building libraries\n-arm"
clickable build-libs
echo "-amd64"
clickable build-libs -a amd64
echo "-arm64"
clickable build-libs -a arm64

#build project for desktop
echo "Building project and run for desktop"
clickable desktop
