[![OpenStore](https://img.shields.io/badge/Install%20from-OpenStore-000000.svg)](https://open-store.io/app/gelek.cibersheep)

# Gelek
Level 9 interpreter using Qt and remglk implementation for Ubuntu Touch OS

## About
App to be able to play interactive fiction games from Level 9 Computing. Import games, manage save files, read company info, instructions for all games, access to most common action on bottom edge menu (or right side if in convergence mode).

## Changelog

1.3.1
- Quick fix. Saved games list was empty
- App in Full Screen
- Added arm64 build

1.3.0
- Added initial support for .tar files (works well for old games)
 - Better image handling. Should be slightly faster with big images
 - Better game images size. Hide UbuntuShape if we have no image
 - Added System Theme option
 - Added switch in settings to show debug information (careful it can be looooong)
 - Fixed old function on BottomEdge
 - Initial support to export svg (in the future)

1.2.3
Backend improvements, code cleaning, simplified qml elements, used filter models, fixed first time game import was not visible, minimal debugging log.

1.2.1 Implemented glk timers fixing the issues with The Archers type of games.
Fixed automatic scrolling hiding the recent text.
Inner improvements (use of StandardPaths, remove personal names from translatable strings, cleaning of the code, etc)

1.2.0 Added dark theme, icon command menu for smaller screens, moved list of saved games to sqlite, slightly improved saved games list page, more accessibility improvements (for keyboard navigation), tweaked game stories section, removed redundant Flickable + ListView and other minor UI improvements

1.0.6 Small UI fixes, added delete option for imported games, new icon for games, word wrapp for titles, ...

1.0.5 Bug fixes: Images clear properly, input area converges correctly, slightly bigger images, better accessibility main page 

1.0.4 Little bug fix: Fixed word suggestion and spell check interference with input TextFile

1.0.3 Support for game images (Vivid and Xenial) and convergence settings. Fixed bug were game imported was found only after foing back Separate image files are not automatically detected jet you have to imported as «new game». Image file name should match the game name file.

1.0.2 Xenial release. Initial support for graphics (adding additional graphics file sometimes are needed:buggy)

1.0.0 Initial release

## Links
Gelek Development: Joan CiberSheep

Support: https://t.me/UTInteractiveFiction

Code Used from Erkyrath remglk (MIT) https://github.com/erkyrath/remglk

Code Used from Zarf glk (MIT) http://eblong.com/zarf/glk/ 

Code Used from Glen Summers and contributors Level 9 interpreter (GPLv2)", license: "GPL 2.0 https://github.com/DavidKinder/Level9 

Code Used from Simon Baldwin glk Level 9 interpreter port (GPLv2) https://github.com/DavidKinder/Level9 

Code Used from WakeRealityDev Qt glk implementation (BSD-2-Clause) https://github.com/WakeRealityDev/Thunderquake_Qt 

---

Special Thanks to Jonatan «Jonny» for his help setting up libraries build tool chain https://gitlab.com/jonnius/

Special Thanks to Brian Douglass for his help with the code, the language and concept, and beta testing http://bhdouglass.com/ 

Special Thanks to Paul Jujuyeh for his additional C++ code and support http://patreon.com/jujuyeh 

Special Thanks to Mikel Larrea for his beta testing and feed back https://github.com/LarreaMikel 

Images from games and manuals are copyrighted by their authors.

App Icon CC-By Kennen Yordle https://thenounproject.com/kennen.yordle/ 

Computer Icon CC-By Matthew Hawdon https://thenounproject.com/matthawdon/ 

Save Icon Base CC-By i cons https://thenounproject.com/iconsguru/ 

## License

Copyright (C) 2019  Joan CiberSheep

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
