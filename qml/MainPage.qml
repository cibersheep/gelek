import QtQuick 2.9
import Ubuntu.Components 1.3

import GelekBackend 1.0
import "components"
import Ubuntu.Content 1.3

import Qt.labs.folderlistmodel 2.1

Page {
    anchors.fill: parent

    header: GelekHeader {
        id: pageHeader
        title: "Gelek"
        flickable: flickable
    }

    Component.onCompleted: {
        //Fixes: No games on the list after first import
        importedGamesModel.rootFolder = gamesPath
        importedGamesModel.folder = gamesPath
    }

    Flickable {
        id: flickable
        anchors {
            fill: parent
            horizontalCenter: parent.horizontalCenter
        }

        //TODO: ¿? contentHeight: contentItem.childrenRect.height
        contentHeight: contentItem.childrenRect.height + units.gu(8)

        Column {
            id: addGame
            width: parent.width

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: pageHeader.height + units.gu(2)
            }

            spacing: units.gu(1)

            Icon {
                id: importGame
                width: units.gu(6)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                name: "add"
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Import")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: Theme.palette.normal.backgroundText
                textSize: Label.XLarge
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("a new game")
                color: Theme.palette.normal.backgroundText
            }

            Text {
                width: parent.width - marginColumn * 12
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Accepted formats: .atr, .bin, .com, .dat, .l9, .sna, .st, .tap, .tzx. Files can be in a .tar file")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: UbuntuColors.graphite
            }
        }

        MouseArea {
            anchors.fill: addGame

            onClicked: {
                var importPage = mainPageStack.push(
                    Qt.resolvedUrl("ImportPage.qml")
                )

                importPage.imported.connect(function(fileUrl) {
                    level9Game  = fileUrl
                })
            }
        }

        Column {
            id: previousGames
            width: parent.width

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: addGame.bottom
                topMargin: units.gu(10)
            }

            Label {
                height: units.gu(4)
                anchors.horizontalCenter: parent.horizontalCenter
                text: importedGamesModel.count === 0
                    ? i18n.tr("No Imported Games Yet")
                    : i18n.tr("Previouly Imported Games")
                color: lightColor
            }

            ListView {
                id: importGameList
                width: parent.width
                interactive: false

                anchors.horizontalCenter: parent.horizontalCenter
                height: units.gu(7) * importedGamesModel.count

                model: importedGamesModel
                delegate: importedGamesDelegate
            }

            FolderListModel {
                id: importedGamesModel
                //This sould be /home/phablet/.cache/gelek.cibersheep/Games/
                rootFolder: gamesPath
                folder: gamesPath
                nameFilters: [
                    "*.sna", "*.dat", "*.tap", "*.l9", "*.st", "*.atr", "*.com", "*.bin", "*.tzx", "*.tar",
                    "*.SNA", "*.DAT", "*.L9", "*.ST", "*.ATR", "*.BIN", "*.COM", "*.TAP", "*.TZX", "*.TAR"
                ]
                showHidden: true
                showDirs: false
                sortField: FolderListModel.Time
            }

            GameListView {
                id: importedGamesDelegate
                // Use GelekBackend.listFiles() or FolderListModel // text: name or text: fileName; link or filePath
                //TODO: Remember to add additional files (image files) and / or rename properly
            }
        }

        Column {
            id: otherOptions
            width: parent.width

            anchors {
                top: previousGames.bottom
                leftMargin: marginColumn
                rightMargin: marginColumn
                topMargin: units.gu(10)
            }

            Divider {}

            ListItem {
                width: parent.width
                divider.visible: true
                GelekItemLayout {
                    title.text: i18n.tr("Saved Games")
                }
                onClicked: {
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("SavedGamesPage.qml"));
                }
            }

            ListItem {
                width: parent.width
                divider.visible: true
                GelekItemLayout {
                    title.text: i18n.tr("Level 9 Game Stories")
                }
                onClicked: {
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("StoriesLevel9Index.qml"));
                }
            }

            ListItem {
                width: parent.width
                divider.visible: true

                GelekItemLayout {
                    title.text: i18n.tr("Level 9 Computing History")
                }

                onClicked: {
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("HistoryLevel9.qml"));
                }
            }
        }
    }
}
