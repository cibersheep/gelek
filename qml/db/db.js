/*
 * uNav http://launchpad.net/unav
 * Copyright (C) 2015 JkB https://launchpad.net/~joergberroth
 * Copyright (C) 2015 Marcos Alvarez Costales https://launchpad.net/~costales
 *
 * uNav is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * uNav is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
// Thanks http://askubuntu.com/questions/352157/how-to-use-a-sqlite-database-from-qml


function openDB() {

    var db = LocalStorage.openDatabaseSync("savedGamesList_db", "0.1", "Saved Games List", 1000);

    try {
        db.transaction(function(tx){
            tx.executeSql('CREATE TABLE IF NOT EXISTS listofsavedgames( revisionDate DATE, id INTEGER PRIMARY KEY, gameName TEXT, savedGameFile TEXT )');
        });
    } catch (err) {
        console.log("Error creating table in database: " + err)
    } return db
}

// Save Game to List
function storeSavedGame(revisionDate, gameName, savedGameFile) {
    var db = openDB();

    db.transaction(function(tx){
        tx.executeSql('INSERT OR REPLACE INTO listofsavedgames(revisionDate, gameName, savedGameFile) VALUES(?, ?, ?)', [revisionDate, gameName, savedGameFile]);
    });
}

function getGameFromSaved(savedGameName) {
    var game = "";
    var date = "";
    var db = openDB();

    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT revisionDate, gameName FROM listofsavedgames WHERE savedGameFile=? ORDER BY revisionDate DESC;', [savedGameName]);

        if (rs.rows.length > 0) {
            game = rs.rows.item(0).gameName;
            date = rs.rows.item(0).revisionDate;
        } else {
            game = "Unkown Game";
            date = null;
            console.log("getGameFromSaved: game name set to Unkown");
        }
    });

    return [game, date];
}

function deleteSavedGameFromList(savedGameName){
    var db = openDB();

    try {
        db.transaction(function(tx){
            tx.executeSql('DELETE FROM listofsavedgames WHERE savedGameFile=?;', [savedGameName]);
        });
    } catch (err) {
        console.log("Error deleting row from database: " + err)
    }
}
