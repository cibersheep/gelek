import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.folderlistmodel 2.1

import "components"
import "components/CommonActions"
import GelekBackend 1.0

Page {
    id: playLevel9

    property string game
    property string gameName: ""
    property string completeOut
    property var    response
    property bool   bottomEdgeVisible: false
    property bool   nameExists: false
    property bool   renderingImage: false
    property int    canvasWidth: 324
    property int    canvasHeight: 225
    property var    commandsBuffer: []
    property int    oldGenertion: 0
    readonly property int untilTheEnd: -1

    property string svgImage: ""
    property int viewboxw: -1
    property int viewboxh: -1
    property int clipWidth: -1
    property int clipHeight: -1

    anchors.fill: parent

    header: GelekHeader {
        id: levelHeader
        title: i18n.tr("Level 9 interpreter")
    }

    //Start the game and send an init json
    Component.onCompleted: {
        GelekBackend.launch(game)
        GelekBackend.sendCommand('{ "type": "init", "gen": 0, "metrics": { "width":329, "height":697 },  "support": [ "graphics", "graphicswin", "timer" ] }')
    }

    Connections {
        target: GelekBackend

        onStdoutLevel: {
            response = ""
            var stdoutResponse = GelekBackend.readSOL()
            completeOut += stdoutResponse

            //Check if we got a complete json from stdout
            try {
                response = JSON.parse(completeOut);
                completeOut = "";
                formatText(response);
            } catch(e) {
                if (settings.debugging) console.log("Not a json. Waiting for next stdout");
            }
        }
    }

    UbuntuShape {
        id: gameImage
        visible: settings.showPicturesInGame && viewboxw !== -1
        width: isLandscape
            ? settings.pictureToTheRight
                ? Math.min(Math.max(clipWidth, (parent.width  - units.gu(6)) * .45), (parent.height -units.gu(10)) / 3  * (clipWidth/clipHeight) + 6)
                : Math.min(Math.max(clipWidth, (parent.width  - units.gu(6)) * .55), (parent.height -units.gu(10)) / 3  * (clipWidth/clipHeight) + 6)
            : Math.min(Math.max(clipWidth, parent.width - units.gu(2)), (parent.height -units.gu(10))/ 3 * (clipWidth/clipHeight) + 6)
        height: width * (clipHeight/clipWidth) + 6
        backgroundColor: Theme.palette.normal.background

        /*
         * This would make the image to be wider as the text
           width: txt.width
           height: width* (img.height / img.width) -6
         */

        aspect: UbuntuShape.Flat

        anchors {
            horizontalCenter: settings.pictureToTheRight && isLandscape
                ? commonsActionsColumnL9.horizontalCenter
                : levelFlickable.horizontalCenter
            top: levelHeader.visible
                ? levelHeader.bottom
                : parent.top
        }

        sourceFillMode: UbuntuShape.PreserveAspectCrop
        sourceVerticalAlignment: UbuntuShape.AlignVCenter
        sourceHorizontalAlignment: UbuntuShape.AlignHCenter
        sourceScale: Qt.vector2d(Math.min(canvasWidth / clipWidth, canvasHeight / clipHeight), Math.min(canvasWidth / clipWidth, canvasHeight / clipHeight) )

        source: Canvas {
            id: img
            width: canvasWidth
            height: canvasHeight
            //Deprecated canvasWindow
            //canvasWindow: Qt.rect(viewboxw, viewboxh, clipWidth, clipHeight)
        }
    }

    Flickable {
        id: levelFlickable
        width: isLandscape ? parent.width * 0.55 : parent.width
        clip: true

        anchors {
            bottom: inputArea.top
            bottomMargin: units.gu(1)
            //Flickable should anchor at the bottom of the image except that is at the right
            top: gameImage.visible && (!isLandscape || !settings.pictureToTheRight)
                ? gameImage.bottom
                : levelHeader.visible
                    ? levelHeader.bottom
                    : parent.top
        }

        //TODO: use contentHeight: contentItem.childrenRect.height
        contentHeight: txt.height + units.gu(5)

        Column {
            id: levelMainText
            width: parent.width
            spacing: units.gu(2)

            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }

            Text {
                id: txt
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignJustify
                color: Theme.palette.normal.backgroundText
            }
        }
    }

    HideHeader {}

    Row {
        id: inputArea
        spacing: units.gu(1)
        width: isLandscape
            ? parent.width * 0.55  - units.gu(4)
            : parent.width

        anchors {
            left: parent.left
            right: isLandscape ? undefined : parent.right
            bottom: parent.bottom
            margins: units.gu(2)
            bottomMargin: bottomEdge.hint.status == BottomEdgeHint.Locked
                ? bottomEdge.hint.height + units.gu(0.5)
                : units.gu(2)
        }

        TextField {
            id: command
            width: parent.width - instruction.width - units.gu(1)

            //Avoid Language aids to the enable return buttons properly
            //See: https://gitlab.com/ubports-linphone/linphone-simple/issues/16
            inputMethodHints: Qt.ImhNoPredictiveText

            onTextChanged: if (text.length === 1 && response.input[0].type === "char") instruction.clicked()

            onAccepted: {
                instruction.clicked();

                //HACK: Brian's hack to make OSK to not hide after accepting a command
                command.focus=false;
                command.focus=true;
            }
        }

        Button {
            id: instruction
            width: units.gu(4)
            enabled: command.text !== ""
            color: command.text === ""
                ? Theme.palette.disabled.foreground
                : darkColor

            Icon {
                width: units.gu(2)
                anchors.centerIn: parent
                color: instruction.enabled
                    ? lighterColor
                    : Theme.palette.disabled.foregroundText
                name: "keyboard-enter"
            }

            onClicked: {
                commandToBuffer(command.text);
                command.text = "";
            }
        }
    }

    BottomEdgeMenuL9 {
        id: bottomEdge
    }

    CommonActionsColumn {
        id: commonsActionsColumnL9
        visible: isLandscape && settings.showMenuToTheRight
        height: parent.height / 3

        anchors {
            left: levelFlickable.right
            right: parent.right
            top: settings.pictureToTheRight && gameImage.visible
                ? gameImage.bottom
                : levelHeader.visible
                    ? levelHeader.bottom
                    : parent.top
        }
    }

    Connections {
        target: mainView

        onThemeChanged: {
            //HACK: Dirty hack to avoid bottomEdge stops working after changing themes
            bottomEdge.commit();
            bottomEdge.collapse();
        }
    }

    function commandToBuffer(command) {
        //Add the latest command to the buffer
        commandsBuffer.push(command);

        //Make sure that if we don't have a timer, to send the last command to GLK
        if (!retrieveTimer.running) {
            sendCommandToGtk(commandsBuffer.shift());
        }
    }

    function formatText(json) {
        if (settings.debugging) console.log("formatText")

        if (json.content) {
            json.content.forEach(function(x){
                if (x.hasOwnProperty("text")) {
                    //flickText(untilTheEnd);
                    var textWindowHeight = txt.height - levelFlickable.contentY;

                    for (var i = 0; i <= x.text.length -1; i++) {
                        var jsonContent = x.text[i].content

                        if (jsonContent) {
                            for (var j = 0; j <= jsonContent.length -1; j++) {
                                jsonContent[j].text = jsonContent[j].text.replace("&","&amp;")

                                switch (jsonContent[j].style) {
                                    case "header":
                                        txt.text += "<font color='" + darkColor + "'><b>" + jsonContent[j].text + "</b></font><br/><br/>"
                                        if (settings.debugging) console.log(txt.text)
                                        break;
                                    case "subheader":
                                        txt.text += "<font color='" + lightColor +"'><b><i>" + jsonContent[j].text + "</i></b></font></h2>"
                                        break;
                                    case "emphasized":
                                        txt.text += "<i>" + jsonContent[j].text + "</i>"
                                        break;
                                    case "input":
                                        txt.text += "<i>" + jsonContent[j].text + "</i><br/>"
                                        break;
                                    default:
                                        //Hide the "> ". We don't need it in Gelek
                                        if (jsonContent[j].text !== "> ") {
                                            if (j < jsonContent.length -1) {
                                                txt.text += jsonContent[j].text
                                            } else
                                                txt.text += jsonContent[j].text  + "<br/>"
                                        }

                                        if (gameName === "") getNameYear(jsonContent[j].text);
                                }
                            }
                        } else {
                            if (settings.debugging) console.log("Empty line");
                            txt.text += "<br/>";
                        }
                    }

                    //TODO: Make the flick accordinly to the space left in the screen
                    //while (!levelFlickable.atYEnd) {levelFlickable.contentY += units.gu(1)}
                    flickText(textWindowHeight);
                }

                if (x.hasOwnProperty("draw")) {
                    if (settings.debugging) console.log("DRAW: We have an image")
                    var xDraw;
                    var yDraw;
                    var wDraw;
                    var hDraw;

                    for (var j=0; j<x.draw.length; j++){
                        xDraw = 0;
                        yDraw = 0;
                        wDraw = canvasWidth;
                        hDraw = canvasHeight;
                        var ctx = img.getContext("2d");

                        var currentColor = x.draw[j].color
                            ? x.draw[j].color
                            : ''
                        ctx.fillStyle = palette(x.draw[j].color)

                        if (x.draw[j].special == "fill") {
                            if (x.draw[j].hasOwnProperty("x")) {
                                xDraw = x.draw[j].x;
                                wDraw = x.draw[j].width;
                            }

                            if (x.draw[j].hasOwnProperty("y")) {
                                yDraw = x.draw[j].y;
                                hDraw = x.draw[j].height;
                            }

                            //The draw[1] must be the size of the image
                            if (j == 2 && viewboxw == -1) {
                                if (settings.debugging) console.log("j 1 rectangle (should be image size):",wDraw,hDraw)
                                clipWidth = wDraw;
                                clipHeight = hDraw
                                viewboxw = xDraw
                                viewboxh = yDraw
                                if (settings.debugging) console.log("j 1",viewboxw,viewboxh)
                                // Set the clipping area
                            }

                            ctx.fillRect(xDraw, yDraw, wDraw, hDraw);

                            if (settings.exportSvg) {
                                exportImgToSvg('<rect x="'+ xDraw +'" y="'+ yDraw +'" width="'+ wDraw +'" height="'+ hDraw +'" fill="' + currentColor + '"/>');
                            }

                            if (settings.debugging) console.log("DRAW: Current Box Coordinates:", xDraw, yDraw, wDraw, hDraw);
                        }
                    }

                    if (settings.debugging && settings.exportSvg) {
                        console.log(
                        "`--------------------------------------`\n",
                        svgImage,
                        "\n`--------------------------------------`"
                        )
                    }

                    img.requestPaint();
                }
            }) //forEach

            /* Mole and Archers sagas, if played with graphics
             * after selection an option, the game waits for
             * timer updates.
             * It supposed to set a repeating timer. In this way: input
             * from player should go into a pile to avoid conflicting
             * gen number...
             */
            if (json.timer) {
                if (settings.debugging) console.log("Timer interval set to: " + json.timer)
                retrieveTimer.interval = json.timer
                retrieveTimer.start()
            }

            if (json.timer === null) {
                retrieveTimer.stop()
            }
        }

        if (json.windows) {
            if (settings.debugging) console.log("WINDOW: Found windows. Get w and h graphics")

            json.windows.forEach(function(x){
                if (x.graphwidth) {
                    canvasWidth = x.graphwidth;
                    if (settings.debugging) console.log("WINDOW: graphwidth",x.graphwidth)
                }

                if (x.graphwidth) {
                    canvasHeight = x.graphheight;
                    if (settings.debugging) console.log("WINDOW: graphheight",x.graphheight)
                }
            })
        }

        if (json.specialinput) {
            if (json.specialinput.type === "fileref_prompt") {
                GelekBackend.createSavedGamesDir()

                switch (json.specialinput.filemode) {
                    case "read":
                        PopupUtils.open(showPopUpRestore)
                        break;
                    case "write":
                        PopupUtils.open(showPopUpSave)
                        break;
                    default:
                        console.log("DEBUG: json.specialinput =",json.specialinput.filemode)
                }
            }
        }

        //If there's more commands in the buffer and we have no timer running
        //take the next command
        if (commandsBuffer.length > 0 && !retrieveTimer.running) {
            sendCommandToGtk(commandsBuffer.shift());
        }
    }

    function palette(color) {
        switch (color) {
            case "#AFEEEE":     //Blue
                return "#bef2ff";
                break;
            case "#8B5742":     //Maroon
                return "#c44114";
                break;
            case "#5CACEE":     //Dark blue
                return "#19b6ee";
                break;
            case "#EE2C2C":     //Red
                return "#ed3146";
                break;
            case "#EEC900":     //Yellow
                return "#f5d412";
                break;
            case "#43DC80":     //Green
                return "#3eb34f";
                break;
            default:
                return color;
        }
    }

    function multiWords(s,t){
        var a = t.toLowerCase();

        for(var i=0; i<s.length; i++){
            if (a.indexOf(s[i].toLowerCase()) != -1){
                console.log('Game Name Found in the List: ' + s[i]);
                return s[i];
            } else {
                console.log('Game Name Not Found in the List');
            }
        }

        return "";
    }

    //A bit too hacky. Use babel like Gelek-vanilla
    function getNameYear(text) {
        //Check if glk has already detected the game
        if (text.match(/\[/)) {
            gameName = text.replace(/\[|\]/g,"").trim();
        } else {
            //Find the name of the game in the intro text
            gameName = multiWords(["Emerald Isle", "Secret Diary of Adrian Mole", "The Archers", "Erik the Viking", "Dungeon Adventure", "Adventure Quest", "Snowball", "Return to Eden", "Worm in Paradise", "Knight Orc", "Gnome Ranger", "Lords of Time", "Red Moon", "Price of Magik", "Lancelot", "Ingrid's Back", "Scapeghost"],text);
            if (text.match(/\d+/g)) {
                var numbers = text.match(/\d+/g).map(Number);
                for (var j = 0; j < numbers.length; j++) {
                    if (numbers[j] > 1900) {
                        gameName += " " + numbers[j];
                        break;
                    }
                }
            }
        }

        if (settings.debugging) console.log("Debug: gameName = " + gameName);
    }

    function sendCommandToGtk(commandString) {
        //Dirty hack to never run into wrong generation number
        //because Timer goes too quick
        if (commandString !== "" && response.gen !== oldGenertion) {
            if (response.input[0].type === "char") { //Make char input accepted from TextField
                //DEBUG
                if (settings.debugging) console.log("Sending command. Char type --------------------------------------")
                GelekBackend.sendCommand('{ "type":"char", "gen":' + response.gen + ', "window":' + response.input[0].id + ', "value": "' + commandString.slice(0) + '" }')
                if (settings.debugging) console.log("+---- Gen: " + response.input[0].gen + " : " + response.gen + " : "+ oldGenertion)
            } else {
                GelekBackend.sendCommand('{ "type":"line", "gen":' + response.gen + ', "window":' + response.input[0].id + ', "value": "' + commandString + '" }') //Should be command.text.slice(response.input[0].maxlen)
            }

            commandString = ""
        }

        command.focus = true
    }

    //Save
    Component {
        id: showPopUpSave

        SaveDialog {}
    }

    //Restore
    Component {
        id: showPopUpRestore

        RestoreDialog {}
    }

    FolderListModel {
        id: savedGamesModel
        rootFolder: savedGamesPath
        folder: savedGamesPath
        nameFilters: [ "*.geleksave", "*.GELEKSAVE", "*.glksave", "*.GLKSAVE" ]
        showHidden: true
        showDirs: false
        sortField: FolderListModel.Time
    }

    function flickText(upToHere) {
        if (upToHere === untilTheEnd) {
            while (!levelFlickable.atYEnd) {
                levelFlickable.contentY += units.dp(1);
            }
        } else {
            upToHere = upToHere / units.dp(1);
            for (var i = 0; i <= upToHere - units.dp(2); i++) {
                if (levelFlickable.atYEnd)
                    break;

                levelFlickable.contentY += units.dp(1);
            }
        }
    }

    Timer {
        id: alertFade
        interval: 3000

        onTriggered: nameExists = false;
    }

    Timer {
        id: retrieveTimer
        repeat: true

        onTriggered: {
            if (response.gen) {
                if (commandsBuffer.length > 0) {
                    sendCommandToGtk(commandsBuffer.shift());
                    commandsBuffer = [];
                    if (settings.debugging) console.log("Emptying buffer")
                } else {
                    //Dirty hack to never run into wrong generation number
                    //because Timer goes too quick
                    if (oldGenertion !== response.gen) {
                        GelekBackend.sendCommand('{ "type":"timer", "gen":' + response.gen + ' }');
                    }
                }

                oldGenertion = response.gen
            }
        }
    }

    function exportImgToSvg(newLine) {
        svgImage += newLine;
        var currentSvg = 'data:image/svg+xml;utf8,<?xml version="1.0" encoding="UTF-8" standalone="no"?>\
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="'+ canvasWidth +'" height="'+ canvasHeight +'" version="1.1" \
            viewBox="' + viewboxw + ' ' + viewboxh + ' '  + canvasWidth + ' ' + canvasHeight + '">'
             + encodeURIComponent(svgImage) + '</svg>'
    }

    //This stops the Level9 terp and frees the memory
    Component.onDestruction: GelekBackend.stopTerp();
}
