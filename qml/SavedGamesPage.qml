import QtQuick 2.9
import Ubuntu.Components 1.3
import Qt.labs.folderlistmodel 2.1
import Ubuntu.Content 1.3
import QtQuick.LocalStorage 2.0

import GelekBackend 1.0
import "components"
import "db/db.js" as DB

Page {
    id: savedGamesLevel9
    anchors.fill: parent

    header: GelekHeader {
        id: levelSavedGamesHeader
        title: i18n.tr("Level 9 Saved Game")
        flickable: savedGameSnowball
    }

    property int sectionNumber

    ListView {
        id: savedGameSnowball

        anchors {
            fill: parent
            topMargin: units.gu(5)
        }

        header: Column {
            id: savedGameTitle
            spacing: units.gu(2)
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                text: "List of Saved Games"
                font.bold: true
                color: darkColor
                horizontalAlignment: Text.AlignHCenter
                textSize: Label.Large
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: UbuntuColors.graphite
                text: savedGamesModel.count === 0
                    ? "No saved games found"
                    : "Slide for options"
            }
        }

        section {
            property: "category"
            criteria: ViewSection.FullString
            delegate: ListItemHeader {
                title: section
            }
        }

        model: categorizedSavedGamesModel
        delegate: savedGameSnowballDelegate
    }

    Component {
        id: savedGameSnowballDelegate

        ListItem {
            property string wichGame;
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            leadingActions:  ListItemActions {
                actions: Action {
                    iconName: "delete"
                    text: i18n.tr("Delete")
                    onTriggered: {
                        GelekBackend.deleteSavedGames(filePath)
                        console.log(fileName)
                        DB.deleteSavedGameFromList(fileName)
                    }
                }
            }

            trailingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "share"
                        text: i18n.tr("Share")
                        onTriggered: {
                            //Open the click with Telegram, OpenStore, etc.
                            var sharePage = mainPageStack.push(
                                Qt.resolvedUrl("SharePage.qml"),
                                {"url": filePath}
                            );
                        }

                    },
                    Action {
                        iconName: "save"
                        text: i18n.tr("Save")
                        onTriggered: {
                            //Save with File Manger, etc.
                            var InstallPage = mainPageStack.push(
                                Qt.resolvedUrl("ExportPage.qml"),
                                {"url": filePath}
                            );
                        }
                    }
                ]
            }

            ListItemLayout {
                id: showItem
                title.text: fileName.replace(".glksave", "")

                Icon {
                    source: "../assets/save.svg"
                    SlotsLayout.position: SlotsLayout.Leading
                    width: units.gu(3)
                }
            }
        }
    }

    FolderListModel {
        id: savedGamesModel
        rootFolder: savedGamesPath
        folder: savedGamesPath
        nameFilters: [ "*.glksave", "*.GLKSAVE" ]
        showHidden: true
        showDirs: false
        sortField: FolderListModel.Time

        //This signals work
        onModelReset: if (settings.debugging) console.log("Model Reset")
        onCountChanged: {
            if (settings.debugging) console.log("Count changed");
            updateModels();
        }

        //File is copied or deleted from the folder
        onDataChanged: {
            if (settings.debugging) console.log("Data changed");
            updateModels();
        }

        onColumnsAboutToBeInserted: if (settings.debugging) console.log("Column about to be inserted")
        onColumnsInserted: if (settings.debugging) console.log("Column inserted")

        function updateModels() {
            if (settings.debugging) console.log("Updating Models")
            categorizedSavedGamesModel.clear()
            var gameName = "";

            for (var i=0; i< savedGamesModel.count; ++i) {
                if (savedGamesModel.get(i, "fileName") !== "gelekSaveList") {
                    gameName = DB.getGameFromSaved(savedGamesModel.get(i, "fileName"))[0];

                    if (gameName == "")
                        gameName = i18n.tr("Unknown");

                    categorizedSavedGamesModel.append({
                        category: gameName,
                        fileName: savedGamesModel.get(i, "fileName"),
                        fileBaseName: savedGamesModel.get(i, "fileBaseName"),
                        filePath: savedGamesModel.get(i, "filePath")
                    })
                }
            }
        }
    }

    ListModel {
        id: categorizedSavedGamesModel
    }

    SortFilterModel {
        id: sortedSavedGamesModel
        model: categorizedSavedGamesModel

        sort {
            property: "category"
            order: Qt.DescendingOrder
        }
    }
}
