import QtQuick 2.9
import Ubuntu.Components 1.3
import "components"

Page {
    id: settingsPage
    anchors.fill: parent

    header: BaseHeader {
        id: pageHeaderSettings
        title: i18n.tr("Settings")
        flickable: settingsFlickable
    }

    Flickable {
        id: settingsFlickable
        anchors.fill: parent
        anchors.top: pageHeaderSettings.bottom

        contentHeight: mainColumnSettings.height + units.gu(8)

        Column {
            id: mainColumnSettings
            width: parent.width - marginColumn * 6
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: units.gu(5)

            anchors {
                top: parent.top
                margins:units.gu(2)
                topMargin: units.gu(6)
            }

            Column {
                width: parent.width
                spacing: units.gu(3)

                Label {
                    text: i18n.tr("General Settings")
                    font.bold: true
                    color: darkColor
                }

                Label {
                    width: parent.width - units.gu(2)
                    text: i18n.tr("Theme")
                }

                ListItem {
                    id: themeTF
                    width: parent.width
                    height: cTheme.height > 0
                        ? cTheme.height
                        : units.gu(7)
                    divider.visible: false
                    highlightColor: mainView.highlightColor

                    property string themeName

                    ChooserTheme {
                        id: cTheme
                        width: parent.width

                        onSelectedThemeChanged: themeTF.themeName = selectedTheme;
                    }
                }
            }

            Column {
                width: parent.width
                spacing: units.gu(3)

                Label {
                    text: i18n.tr("Convergence Settings")
                    font.bold: true
                    color: darkColor
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - cm.width - units.gu(2)
                        text: i18n.tr("Convergence")
                    }

                    Switch {
                        id: cm
                        checked: settings.convergenceMode

                        onClicked: {
                            settings.convergenceMode = !settings.convergenceMode
                        }
                    }
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - pr.width - units.gu(2)
                        text: i18n.tr("Pictures to the right")
                        color: settings.convergenceMode ? Theme.palette.normal.backgroundText : Theme.palette.disabled.backgroundText
                    }

                    Switch {
                        id: pr
                        opacity: settings.convergenceMode ? 1 : 0.5
                        checked: settings.pictureToTheRight

                        onClicked: {
                            settings.pictureToTheRight = !settings.pictureToTheRight
                            //If no pictures to the right  and no menu, there's no convergence option
                            settings.convergenceMode = settings.pictureToTheRight || settings.showMenuToTheRight
                        }
                    }
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - mr.width - units.gu(2)
                        text: i18n.tr("Show menu to the right")
                        color: settings.convergenceMode ? Theme.palette.normal.backgroundText : Theme.palette.disabled.backgroundText
                    }

                    Switch {
                        id: mr
                        opacity: settings.convergenceMode ? 1 : 0.5
                        checked: settings.showMenuToTheRight

                        onClicked: {
                            settings.showMenuToTheRight = !settings.showMenuToTheRight
                            //If no pictures to the right  and no menu, there's no convergence option
                            settings.convergenceMode = settings.pictureToTheRight || settings.showMenuToTheRight
                        }
                    }
                }
            }

            Column {
                width: parent.width
                spacing: units.gu(3)

                Label {
                    text: i18n.tr("Game Settings")
                    font.bold: true
                    color: darkColor
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - sip.width - units.gu(2)
                        text: i18n.tr("Show ingame pictures")
                    }

                    Switch {
                        id: sip
                        checked: settings.showPicturesInGame

                        onClicked: settings.showPicturesInGame = !settings.showPicturesInGame
                    }
                }
            }

            Column {
                width: parent.width
                spacing: units.gu(3)

                Label {
                    text: i18n.tr("Development Settings")
                    font.bold: true
                    color: darkColor
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - edl.width - units.gu(2)
                        text: i18n.tr("Enable debug logs")
                    }

                    Switch {
                        id: edl
                        checked: settings.debugging

                        onClicked: settings.debugging = !settings.debugging
                    }
                }
            }
        }
    }
}

