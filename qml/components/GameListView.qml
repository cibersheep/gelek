import QtQuick 2.9
import Ubuntu.Components 1.3

import GelekBackend 1.0

Component {
    ListItem {
        width: Math.min(parent.width, units.gu(70))
        anchors.horizontalCenter: parent.horizontalCenter
        divider.visible: false
        clip: true
        highlightColor: selectionColor

        leadingActions: ListItemActions {
            actions: Action {
                iconName: "delete"
                text: i18n.tr("Delete")

                onTriggered: {
                    GelekBackend.deleteSavedGames(filePath)
                }
            }
        }

        ListItemLayout {
            id:layout
            title.text: fileName

            Icon {
                source: "../../assets/game.svg"
                SlotsLayout.position: SlotsLayout.Leading
                //color: darkColor
                width: units.gu(3)
            }

            ProgressionSlot {}
        }

        onClicked: mainPageStack.push(
            Qt.resolvedUrl("../PlayLevel9.qml"),
            {"game": filePath}
        )
    }
}
