import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import GelekBackend 1.0

Dialog {
    id: dialogueRestore

    property bool gameRestored: false

    title: i18n.tr("Restore Game")
    text: savedGamesModel.count === 0 ? i18n.tr("There's no saved games yet") : i18n.tr("Choose a saved game to restore")

    ScrollView {
        height: savedGamesModel.count === 0 ? units.gu(1) : units.gu(25)

        ListView {
            id: savedGameList
            width: parent.width
            clip: true

            model: savedGamesModel
            delegate: savedGamesDelegate
        }
    }

    Component {
        id: savedGamesDelegate

        ListItem {
            property string wichGame;
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: fileName

                Icon {
                    source: "../../assets/save.svg"
                    SlotsLayout.position: SlotsLayout.Leading
                    width: units.gu(3)
                }
            }

            onClicked:  {
                GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "' + savedGamesPath + fileName + '" }')
                gameRestored = true
                PopupUtils.close(dialogueRestore)
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: PopupUtils.close(dialogueRestore)
    }

    Component.onDestruction: {
        //Prevent remGlk terp stop when Dialog is dismissed by Esc key stroke
        if (!gameRestored) {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "" }')
        }
    }
}
