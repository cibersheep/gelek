import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: commonActions
    height: parent.height

    property int windRoseWidth: units.gu(4)

    header: Header {
        visible: false
    }

    Grid {
        id: mainGrid
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.topMargin: units.gu(5)
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - units.gu(4)
        spacing: units.gu(2)

        Column {
            width: (parent.width / 3) - units.gu(2)
            anchors.leftMargin: units.gu(2)
            spacing: units.gu(2)

            Button {
                width: parent.width
                color: lightColor
                text: "Examine"
                onClicked: {
                    command.text = "EXAMINE "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Pull"
                onClicked: {
                    command.text = "PULL "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Push"
                onClicked: {
                    command.text = "PUSH "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Open"
                onClicked: {
                    command.text = "OPEN "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Close"
                onClicked: {
                    command.text = "CLOSE "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
        }

        Column {
            width: (parent.width / 3) - units.gu(2)
            anchors.leftMargin: units.gu(2)
            spacing: units.gu(2)
            Button {
                width: parent.width
                color: lightColor
                text: "Get"
                onClicked: {
                    command.text = "GET "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Drop"
                onClicked: {
                    command.text = "DROP "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Wear"
                onClicked: {
                    command.text = "WEAR "
                    bottomEdge.collapse()
                    command.focus = true
                    flickText.start()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Inventory"
                onClicked: {
                    command.text = "INVENTORY"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
                }
            }

            Button {
                width: parent.width
                color: lightColor
                text: "Score"
                onClicked: {
                    command.text = "SCORE"
                    instruction.clicked()
                    bottomEdge.collapse()
                }
            }
        }

        Column {
            width: (parent.width / 3)
            anchors.leftMargin: units.gu(2)
            spacing: units.gu(2)

            Button {
                width: parent.width
                color: lightColor
                text: "Save"
                onClicked: {
                    command.text = "SAVE"
                    sendCommandToGtk()
                    bottomEdge.collapse()
                }
            }
            Button {
                width: parent.width
                color: lightColor
                text: "Restore"
                onClicked: {
                    command.text = "RESTORE"
                    sendCommandToGtk()
                    bottomEdge.collapse()
                }
            }
            Grid {
                height: units.gu(12)
                width: height
                id: navigation
                anchors.horizontalCenter: parent.horizontalCenter

                columns: 3
                //spacing: units.gu(2)
                Icon { id: nw; width: windRoseWidth; height: width; source: "../../assets/bottomedge/nw.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "NW"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }
                Icon { id: up; color: lightColor; width: windRoseWidth; height: width; source: "../../assets/bottomedge/n.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "N"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }
                Icon { id: ne; width: windRoseWidth; height: width; source: "../../assets/bottomedge/ne.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "NE"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }
                Icon { id: w; color: lightColor; width: windRoseWidth; height: width; source: "../../assets/bottomedge/w.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "W"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }

                Icon { id: other; color: lightColor; width: windRoseWidth; height: width; source: "../../assets/bottomedge/centre.svg"}

                Icon { id: e; color: lightColor; width:windRoseWidth; height: width; anchors.leftMargin: units.gu(2); source: "../../assets/bottomedge/e.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "E"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }
                Icon { id: sw; width: windRoseWidth; height: width; source: "../../assets/bottomedge/sw.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "SW"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }
                Icon { id: s; color: lightColor; width: windRoseWidth; height: width; source: "../../assets/bottomedge/s.svg"
                    MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                command.text = "S"
                                instruction.clicked()
                                bottomEdge.collapse()
                                command.focus = true
                           }
                    }
                }
                Icon { id: se; width: windRoseWidth; height: width; source: "../../assets/bottomedge/se.svg"
                    MouseArea {
                        anchors.fill: parent;
                        onClicked: {
                            command.text = "SE"
                            instruction.clicked()
                            bottomEdge.collapse()
                            command.focus = true
                       }
                    }
                }
            }
        }
    }
}
