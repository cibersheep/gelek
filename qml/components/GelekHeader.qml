import QtQuick 2.9
import Ubuntu.Components 1.3

BaseHeader {
    trailingActionBar {
        actions: [
            Action {
                id: actionInfo
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")
                onTriggered: {
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("../About.qml"));
                }
            },

            Action {
                id: actionSettings
                iconName: "settings"
                shortcut: "Ctrl+s"
                text: i18n.tr("Settings")
                onTriggered: {
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("../SettingsPage.qml"));
                }
            }
        ]
    }
}
