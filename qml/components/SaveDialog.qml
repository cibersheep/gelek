import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import QtQuick.LocalStorage 2.0
import "../db/db.js" as DB
import GelekBackend 1.0

Dialog {
    id: dialogue

    property bool nameExists: false
    property bool gameSaved: false

    title: i18n.tr("Save Game")
    text: i18n.tr("Enter a name to save the game")

    Label {
        id: alertText
        horizontalAlignment: Text.AlignCenter
        visible: nameExists
        text: i18n.tr("File name already exists!")
        color: Theme.palette.normal.negative
    }

    TextField {
        id: saveGameName
        onAccepted: saveButton.clicked()
        inputMethodHints: Qt.ImhNoPredictiveText
    }

    Button {
        id: saveButton
        text: i18n.tr("Save")
        color: saveGameName.text ==="" || saveGameName.text === " " ? Theme.palette.disabled.positive : Theme.palette.normal.positive
        enabled: !(saveGameName.text ==="" || saveGameName.text === " ")

        onClicked: {
            nameExists = checkNameExits(saveGameName.text);

            if (saveGameName.text !=="" && saveGameName.text !== " " && !nameExists) {
                console.log("Debug: Game name to save: " + saveGameName.text);
                console.log("Debug: gen", response.gen, "path",mainView.savedGamesPath + saveGameName.text,"gameName",gameName);

                //Add gameid to the savefile
                GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "' + mainView.savedGamesPath + saveGameName.text + '.glksave", "gameid": "' + gameName + '" }')
                DB.storeSavedGame(Qt.formatDateTime(new Date(), "dd-MM-yyyy, HH:mm:ss"), gameName, saveGameName.text + '.glksave')
                gameSaved = true
                PopupUtils.close(dialogue)
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: PopupUtils.close(dialogue)
    }

    function checkNameExits(gameNameText) {
        var itExists = false;

        for (var i=0; i < savedGamesModel.count; i++){
            if (savedGamesModel.get(i, "fileBaseName") === gameNameText) {
                console.log("Debug: File name exists");
                itExists = true;
                alertFade.start();
                break;
            }
        }

        return itExists;
    }

    Timer {
        id: alertFade
        interval: 3000

        onTriggered: nameExists = false
    }

    Component.onDestruction: {
        //Prevent remGlk terp stop when Dialog is dismissed by Esc key stroke
        if (!gameSaved) {
             GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "" }')
        }
    }
}
