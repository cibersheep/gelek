import QtQuick 2.9
import Ubuntu.Components 1.3

import "CommonActions"

BottomEdge {
    height: units.gu(38)
    width: parent.width

    hint {
        text: i18n.tr("Actions")
        deactivateTimeout: 300
        enabled: !isLandscape || !settings.showMenuToTheRight
        visible: !isLandscape || !settings.showMenuToTheRight
    }

    preloadContent: true

    onCollapseStarted: bottomEdgeVisible = false

    onCollapseCompleted: flickText()

    onCommitStarted: {
        bottomEdgeVisible = true
        command.focus = false
    }

    contentComponent: Page {
        id: commonActions
        width: bottomEdge.width
        height: bottomEdge.height

        header: BaseHeader {
            id: levelBottomEdgeHeader
            title: i18n.tr("Common Actions")
        }

        CommonActionsL9 {
            anchors.top: levelBottomEdgeHeader.bottom
        }
    }
}
