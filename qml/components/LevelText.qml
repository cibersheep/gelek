import QtQuick 2.9
import Ubuntu.Components 1.3

Text {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    horizontalAlignment: Text.AlignJustify
    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    color: Theme.palette.normal.backgroundText
}
