import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: commonActions
    //height: parent.height

    property int windRoseWidth: units.gu(4)
    anchors.top: gameImage.visible && settings.pictureToTheRight ? gameImage.bottom : levelHeader.visible ? levelHeader.bottom : parent.top

    header: Header {
        visible: false
    }

    Component {
        id: fullTextActions

        CommonActionsL9 {
            height: commonActions.height
        }
    }

    Component {
        id: iconsActions

        CommonActionsL9Icons {
            height: commonActions.height
        }
    }

    Loader {
        id: fullActions
        width: parent.width - units.gu(4)
        active: parent.height > units.gu(31)
        sourceComponent: fullTextActions

        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
    }

    Loader {
        width: parent.width - units.gu(4)
        active: !fullActions.active
        sourceComponent: iconsActions

        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
    }
}
