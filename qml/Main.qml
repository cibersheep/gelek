import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0

import Qt.labs.settings 1.0
import Qt.labs.platform 1.0
import GelekBackend 1.0
import "components"
import "db/db.js" as DB

MainView {
    id: mainView
    objectName: "mainView"
    applicationName: "gelek.cibersheep"

    width: units.gu(40)
    height: units.gu(75)

    property string savedGamesPath
    property string gamesPath

    //Margins
    property int marginColumn: units.gu(1)

    //Colors
    property string lightColor: "#90435e"
    property string darkColor: "#842244"
    property string lighterColor: UbuntuColors.porcelain
    property bool darkTheme: theme.name === "Ubuntu.Components.Themes.SuruDark"
    property string selectionColor: darkTheme
        ? "#8e436a"
        : "#f3eaed"
    property string level9Game: ""

    //Settings properties
    property var settings: Settings {
        property bool convergenceMode: true
        property bool pictureToTheRight: false
        property bool showMenuToTheRight: true
        property bool showPicturesInGame: true
        property bool oldSavedGameListExported: false
        property string themeName: "Ubuntu.Components.Themes.Ambiance"
        property bool haveCreatedFolders: false
        property bool debugging: false
        property bool exportSvg: false
    }

    property bool isLandscape: settings.convergenceMode && width > height  + units.gu(10) // Think about the keyboard

    signal themeChanged()

    anchorToKeyboard: true

    PageStack {
        id: mainPageStack
        anchors.fill: parent

        Component.onCompleted: {
            theme.name = settings.themeName;
            upgradeData();

            if (!settings.haveCreatedFolders) {
                GelekBackend.firtRunFolderSetUp();
                settings.haveCreatedFolders = true;
            }

            var cachePath = String(StandardPaths.writableLocation(StandardPaths.CacheLocation))
            cachePath = cachePath.replace("file://","")
            savedGamesPath = cachePath + "/SavedGames/"
            gamesPath = cachePath + "/Games/"
            mainPageStack.push(pageMain);
        }
    }

    Component {
        id: pageMain

        MainPage {}
    }

    Component {
        id: importingMessage

        Rectangle {
            anchors.fill: parent
            color: "#eee"

            Text {
                text: i18n.tr("Importing List of Saved Games")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                width: parent.width

                anchors {
                    horizontalCenter: activity.horizontalCenter
                    bottom: activity.top
                    bottomMargin: units.gu(4)
                }
            }

            ActivityIndicator {
                id: activity
                anchors.centerIn: parent
                running: true
            }
        }
    }

    Loader {
        id: busyLoader
        anchors.fill: parent
    }

    function upgradeData() {
        if (!settings.oldSavedGameListExported) {
            busyLoader.sourceComponent = importingMessage;
            console.log("Exporting old saveGameList");
            var listOfSavedGames = GelekBackend.exportSave();
            listOfSavedGames = listOfSavedGames.toString().trim().split("},");

            for (var i=0; i < listOfSavedGames.length; i++) {
                if (listOfSavedGames[i].toString() == "") continue;
                var name = listOfSavedGames[i].toString().split(": { ")[0].replace(/\"/g,"");
                var info = listOfSavedGames[i].toString().split(": { ")[1].replace(/\"/g,"");
                var game = info.slice(5,info.indexOf(","));
                var time = info.slice(info.indexOf("time:") + 5).trim();
                //DEBUG: console.log("savedGame " + i + " info " + info + "\n time " + time);
                DB.storeSavedGame(time.toString(), game.toString(), "", name.toString());
            }

            settings.oldSavedGameListExported = true;
            busyLoader.sourceComponent = undefined;
        }
    }
}
